import numpy as np
import pandas as pd
from sklearn import linear_model
from scipy.optimize import curve_fit
from pymongo import MongoClient
import heapq
from joblib import Parallel, delayed
import multiprocessing
import math
import dill
import pathos.multiprocessing as mp

client = MongoClient()
db = client.citations_project

def gompertz(t, N0, K, r, T):
    return K * np.exp(-T * np.exp(-r * t))

class AuthorForecaster(object):
    def __init__(self):
        print "Initializing author forecaster"
        pass
    
    def predict_number_publications(self, ts, num_points_foreacast=30, step_size=1):
        mean = np.mean(ts.values)
        st = np.std(ts.values)
        if st<=0:
            st=1
        ts_values = [int(a) for a in np.random.normal(loc=mean, scale=st, size=num_points_foreacast)]
        ts_values=np.clip(a=ts_values, a_min=0, a_max=np.inf)
        ts_index = [t for t in np.arange(ts.index[-1] + step_size, ts.index[-1] + (num_points_foreacast + 1)*step_size, step=step_size)]
        return pd.DataFrame().from_records(zip(ts_index, ts_values), columns=['time', 'values'], index=['time'])
    
    def predict_number_citations(self, ts, num_points_foreacast=30, step_size=1):
        mean = np.mean(ts.values)
        st = np.std(ts.values)
        if st<=0:
            st=1
        ts_values = [int(a) for a in np.random.normal(loc=mean, scale=st, size=num_points_foreacast)]
        ts_values=np.clip(a=ts_values, a_min=0, a_max=np.inf)
        ts_index = [t for t in np.arange(ts.index[-1] + step_size, ts.index[-1] + (num_points_foreacast + 1)*step_size, step=step_size)]
        return pd.DataFrame().from_records(zip(ts_index, ts_values), columns=['time', 'values'], index=['time'])
    
    def interpolate_citations(self, ts):
        ts_values = np.ones(ts.index[1] - ts.index[0] -1) * 3
        ts_ix = np.arange(ts.index[0] + 1, ts.index[1])
        return pd.DataFrame().from_records(zip(ts_ix, ts_values), columns=['time', 'values'], index=['time'])

    def predict_using_last_5_nos(self, ts, num_points_foreacast=30, step_size=1):
        print "inside predict_using_last_5_nos"
        print ts
        available_points = len(ts.values)
        print available_points
        regr = linear_model.LinearRegression()
        print regr
        new_ts = ts.values.flatten()
        print new_ts
        for i in xrange(0, num_points_foreacast):
            ys = new_ts[max(0,i+available_points-5 + 1):i+available_points]
            xs = [n for n in xrange(1, len(ys)+1)]
            regr.fit(np.array(xs).reshape(-1, 1), np.array(ys).reshape(-1, 1));
            new_y = max(regr.predict(max(xs) + 1), 0)
            new_ts = np.append(new_ts, new_y)
        print "predicted series:",new_ts
        new_ts = new_ts[available_points:]
        ts_index = [t for t in np.arange(ts.index[-1] + step_size, ts.index[-1] + (num_points_foreacast + 1)*step_size, step=step_size)]
        return pd.DataFrame().from_records(zip(ts_index, new_ts), columns=['time', 'values'], index=['time'])

    def smoothen(ts):
        smoothened_array = []
        for i in range(0, len(ts)):
            subarray = ts[max(0,i-2):min(len(ts),i+3)]
            smoothened_array.append((float)(sum(subarray)/len(subarray)))
        return smoothened_array


    def predict_using_gompertz(self, ts, step_size = 1):
        # print "ts:",ts
        available_points_len = len(ts.values)
        time_points = ts.values.flatten()# smoothen(list(ts.values.flatten()))
        print "smoothened:",time_points
        time_points_cumulative = np.cumsum(time_points)

        # hardcoding for now
        LB = [1.1699999999999999, 144.6872480297902, 0.043574494960807508, 3.8002045068185986]
        UB = [1.8500000000000001, 289.08479921611081, 0.14468181009951403, 5.3294351195006451]
        meanNOE = 1.4849499999999971
        meanKE = 200.67182305697051
        meanRE = 0.056053333139626321
        meanTE = 4.3838690068943906

        try:
            popt, cov = curve_fit(gompertz, np.arange(1, available_points_len+1),
                                  time_points_cumulative,
                                  (meanNOE, meanKE, meanRE, meanTE),
                                  bounds=(LB, UB))
            N0, K, r, T = popt
        except Exception, e:
            print "Exception!",e
            N0 = meanNOE
            K = meanKE
            r = meanRE
            T = meanTE

        cumulative_preds = []
        prev_value = gompertz(available_points_len, N0, K, r, T) #time_points_cumulative[available_points_len-1]
        cumulative_preds.append(prev_value)
        print "before prediction"
        for year in np.arange(available_points_len+1, 100+1):
            prediction = gompertz(year, N0, K, r, T) #int(round(gompertz(year, N0, K, r, T)))
            if prediction < prev_value:
                prediction = prev_value
            else:
                prev_value = prediction
            cumulative_preds.append(prediction)

        print "cumulative_preds:",cumulative_preds
        preds = np.diff(cumulative_preds)

        mean_ts = np.mean(time_points)
        std_ts = np.std(time_points)

        coeff_variation = float(std_ts) / mean_ts

        preds = [int(round(a)) for a in preds]
        randomized_predictions = []
        zero_count = 0

        for pred in preds:
            if zero_count == 1 and pred == 0:
                randomized_predictions.append(0)
                break
            else:
                zero_count = 0
            if pred == 0:
                randomized_predictions.append(0)
                zero_count += 1
                continue

            new_std = coeff_variation * pred / 2
            new_pred = int(round(np.random.normal(pred, new_std, 1)[0]))
            randomized_predictions.append(max(0, new_pred))

        print "randomized_predictions:",randomized_predictions
        num_points_foreacast = len(randomized_predictions)

        ts_index = [t for t in np.arange(ts.index[-1] + step_size, ts.index[-1] + (num_points_foreacast + 1)*step_size, step=step_size)]

        print "ts_index:",ts_index
        return pd.DataFrame().from_records(zip(ts_index, randomized_predictions), columns=['time', 'values'], index=['time'])

    def get_nearest_k(self, requested_author_id, complete_ts, prof_tags):
        threshold = int(math.ceil(1*float(len(prof_tags))/4))
        print "prof_tags:",prof_tags
        good_ones = db.cleaned_profilesV2.aggregate(
            [ 
                { "$match": { "tags."+str(threshold): { "$exists": True } } }, 
                { "$redact": { 
                    "$cond": [ 
                        { "$gte": [ 
                            { "$size": { "$setIntersection": [ "$tags", prof_tags ] } }, 
                            threshold
                        ]},
                        "$$KEEP", 
                        "$$PRUNE" 
                    ]
                }}
            ]
        )
        original_list_good_ones = list(good_ones)
        print "len(original_list_good_ones):",len(original_list_good_ones)
        ts = complete_ts[:20]
        top_1000_from_good_ones = heapq.nlargest(500, original_list_good_ones, key=lambda l: l["citation_count"])

        def DTWDistance(s1, s2, w):
            DTW={}

            w = max(w, abs(len(s1)-len(s2)))
            for i in range(-1,len(s1)):
                for j in range(-1,len(s2)):
                    DTW[(i, j)] = float('inf')
            DTW[(-1, -1)] = 0
            for i in range(len(s1)):
                for j in range(max(0, i-w), min(len(s2), i+w)):
                    dist= (s1[i]-s2[j])**2
                    DTW[(i, j)] = dist + min(DTW[(i-1, j)],DTW[(i, j-1)], DTW[(i-1, j-1)])
            return math.sqrt(DTW[len(s1)-1, len(s2)-1])

        def processInput(g):
            if g["_id"] != requested_author_id:
                if g["mean_citation_count"] != 0:
                    approx_pubs = int(round(float(g["citation_count"])/g["mean_citation_count"]))
                else:
                    approx_pubs = 0
                return  [DTWDistance(ts, g["ts"], 3), g["_id"], g["ts"], g["author_name"], g["citation_count"], g["author_aff"],str(round(g["mean_citation_count"],2)),
                        g["tags_str"],approx_pubs]

        num_cores = multiprocessing.cpu_count()
        p = mp.Pool(num_cores)

        distances = p.map(lambda x: processInput(x), top_1000_from_good_ones)

        distances = filter(lambda x: x is not None, distances)
        # # here distances include the list of tuples of for [distance, profile_id, timeseries, author_name, citation_count]
        # # and these are arranged by citation_count in descending order
        nearest_k = heapq.nsmallest(5, distances, key=lambda l: l[0])
        return nearest_k

    def predict_using_gompertz_old(self, ts, step_size = 1):
        print "ts:",ts
        available_points_len = len(ts.values)
        time_points = ts.values.flatten()# smoothen(list(ts.values.flatten()))
        print "smoothened:",time_points
        time_points_cumulative = np.cumsum(time_points)

        # hardcoding for now
        LB = [1.1699999999999999, 144.6872480297902, 0.043574494960807508, 3.8002045068185986]
        UB = [1.8500000000000001, 289.08479921611081, 0.14468181009951403, 5.3294351195006451]
        meanNOE = 1.4849499999999971
        meanKE = 200.67182305697051
        meanRE = 0.056053333139626321
        meanTE = 4.3838690068943906

        try:
            popt, cov = curve_fit(gompertz, np.arange(1, available_points_len+1),
                                  time_points_cumulative,
                                  (meanNOE, meanKE, meanRE, meanTE),
                                  bounds=(LB, UB))
            N0, K, r, T = popt
        except Exception, e:
            print "Exception!",e
            N0 = meanNOE
            K = meanKE
            r = meanRE
            T = meanTE

        cumulative_preds = []
        prev_value = gompertz(0, N0, K, r, T) #time_points_cumulative[available_points_len-1]
        cumulative_preds.append(prev_value)
        print "before prediction"
        for year in np.arange(1, 100+1):
            prediction = gompertz(year, N0, K, r, T) #int(round(gompertz(year, N0, K, r, T)))
            if prediction < prev_value:
                prediction = prev_value
            else:
                prev_value = prediction
            cumulative_preds.append(prediction)
        print "cumulative_preds:",cumulative_preds
        preds = np.diff(cumulative_preds)

        mean_ts = np.mean(time_points)
        std_ts = np.std(time_points)

        coeff_variation = float(std_ts) / mean_ts

        preds = [int(round(a)) for a in preds]
        randomized_predictions = []
        zero_count = 0
        for pred in preds:
            if zero_count == 1 and pred == 0:
                randomized_predictions.append(0)
                break
            else:
                zero_count = 0
            if pred == 0:
                randomized_predictions.append(0)
                zero_count += 1
                continue

            new_std = coeff_variation * pred / 2
            new_pred = int(round(np.random.normal(pred, new_std, 1)[0]))
            randomized_predictions.append(max(0, new_pred))
        print "randomized_predictions:",randomized_predictions
        num_points_foreacast = len(randomized_predictions)
        ts_index = [t for t in np.arange(ts.index[0] , ts.index[0] + (num_points_foreacast)*step_size, step=step_size)]

        print "ts_index:",ts_index
        return pd.DataFrame().from_records(zip(ts_index, randomized_predictions), columns=['time', 'values'], index=['time'])






