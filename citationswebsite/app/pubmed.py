from Bio import Entrez
import StringIO
import requests
import sys
import time

base_url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/"

def get_publications(author_name):
	complete_list = []
	while True:
		retstart = 0
		url = base_url + "esearch.fcgi?db=pubmed&retmax=1000&retstart="+str(retstart)+"&term="+author_name
		response = requests.get(url, timeout=10)
		s = StringIO.StringIO(response.content)
		records = Entrez.read(s)
		complete_list.extend(records['IdList'])
		records_returned = int(records['Count'])
		max_records = int(records['Count'])
		if records_returned <= max_records:
			break;
		else:
			retstart = max_records + 1
	return complete_list

def get_publication_details(ids):
	url = base_url + "efetch.fcgi?db=pubmed&rettype=abstract&id="
	ids_str = ",".join(map(str,ids))
	url = url + ids_str
	response = requests.get(url, timeout=10)
	s = StringIO.StringIO(response.content)
	records = Entrez.read(s)
	pub_details = {}
	for record in records:
		details = {}
		details['id'] = record['MedlineCitation']['PMID'].title()
		details['year'] = record['MedlineCitation']['DateCreated']['Year']
		details['title'] = record['MedlineCitation']['Article']['ArticleTitle']
		pub_details[details['id']] = details
	return pub_details

def get_publication_details_batched(ids, batch_size=100):
	whole_pub_details = {}
	while ids:
		if len(ids) <= batch_size:
			selected_ids = ids
			ids=[]
		else:
			selected_ids = ids[0:batch_size-1]
			ids=ids[batch_size-1:]
		pub_details = get_publication_details(selected_ids)
		whole_pub_details.update(pub_details)
		print str(len(ids)) + " more to be fetched."
		time.sleep(5)
	return whole_pub_details


def extract_citations(record):
    papers_citing = []
    s_ind  = [ind for ind, rec in enumerate(record['LinkSetDb']) if rec['LinkName'] == 'pubmed_pubmed_citedin']
    if len(s_ind):
        s = record['LinkSetDb'][s_ind[0]]
        papers_citing = [item['Id'] for item in s['Link']]
    return (record['IdList'][0], papers_citing)

# Usually we pass a set of ids. all_citation_papers contains a complete list of papers
# which is found across different individual publications in citations
def get_citations(ids):
	url = base_url + "elink.fcgi?db=pubmed"
	for id in ids:
		url = url + "&id={}".format(str(int(id)))
	response = requests.get(url, timeout=10)
	s = StringIO.StringIO(response.content)
	records = Entrez.read(s)
	all_citation_papers = []
	citations = {}
	for record in records:
		citation_list = extract_citations(record);
		citations[citation_list[0]] = citation_list[1]
		all_citation_papers.extend(citation_list[1])
	return [citations, all_citation_papers]


def get_citations_per_pub(citation_list):
	citation_per_pub = {}
	for key,value in citation_list.iteritems():
		citation_per_pub[key] = len(value)
	return citation_per_pub

def get_yearwise_citation_counts(pub_details, citation_list):
	year_wise = {}
	for key,value in citation_list.iteritems():
		for paper in value:
			year = pub_details[paper]['year']
			if year in year_wise:
				year_wise[year] = year_wise[year] + 1
			else:
				year_wise[year] = 1

	year_wise_tuples = []
	for key,value in year_wise.iteritems():
		year_wise_tuples.append([key, value])

	year_wise_tuples = sorted(year_wise_tuples, key=lambda x: x[0])
	return year_wise_tuples

def get_citations_ts(pub_id):
	[citations_list, citation_holders] = get_citations([pub_id])
	pub_details = get_publication_details([pub_id])
	citation_holders_details = get_publication_details_batched(citation_holders)
	year_wise = {}
	for pub in citation_holders:
		year = citation_holders_details[pub]['year']
		try:
			year_wise[year]+=1
		except KeyError:
			year_wise[year]=1
	ts = []
	for key,val in year_wise.iteritems():
		ts.append((int(key), int(val)))
	original_paper_name = pub_details[pub_id]['title']
	return [original_paper_name, sorted(ts, key=lambda x: x[0])]

def fetch(author_name):
	print "Going to fetch :" + author_name
	pub_ids = get_publications(author_name)
	print "Publications"
	print pub_ids
	pub_details = get_publication_details(pub_ids)
	print "Publication details"
	print pub_details
	[citations_list, citation_holders] = get_citations(pub_ids)
	citations_per_pub = get_citations_per_pub(citations_list)
	print "Citations per pub"
	print citations_per_pub
	complete_list = []
	for key_id in pub_ids:
		complete_list.append([pub_details[key_id]['title'], citations_per_pub[key_id], int(pub_details[key_id]['year']), key_id])
	print "Complete list"
	print complete_list
	complete_list = sorted(complete_list, key=lambda x: -1*x[1])
	return complete_list

def main():
	complete_list = fetch("Andrew Ng")
	print complete_list

def main123():
	author_name = "Andrew Ng"
	pub_ids = get_publications(author_name)
	print "Author :" + author_name
	print "Publications :"
	print pub_ids
	pub_details = get_publication_details(pub_ids)
	print "Publication Years"
	print pub_details
	[citations_list, citation_holders] = get_citations(pub_ids)
	print "Total number of secondary papers to be fetched " + str(len(citation_holders))
	citation_holders_details = get_publication_details_batched(citation_holders)
	pub_details.update(citation_holders_details)
	print "Citations List"
	print citations_list
	citations_time_series = get_yearwise_citation_counts(pub_details, citations_list)
	print "Time Series"
	print citations_time_series

if __name__ == "__main__":
	sys.exit(main())


