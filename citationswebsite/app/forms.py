import flask_wtf
from flask_wtf import Form
from wtforms import StringField, PasswordField, RadioField
from wtforms.validators import DataRequired, Email

class QueryForm(Form):
	author_name = StringField('Enter name of author ', validators=[DataRequired()])
	source = RadioField('', choices=[('scholar','Google Scholar'),('pubmed','Pubmed')], validators=[DataRequired()])

class PaperDetailsHiddenForm(Form):
	paper_link = StringField('', validators=[DataRequired()])
