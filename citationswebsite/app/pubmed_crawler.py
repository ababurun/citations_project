import pubmed
import sys
import random
import time
from pymongo import MongoClient

client = MongoClient()
db = client.citations_project

def main():

	while True:
		profiles_left = db.profiles.find({"$and":[{ "pubmed_crawledv2" : { "$exists" : False }}, {"citation_count":{"$gt":1}}]}).count()
		if profiles_left == 0:
			break
		print "Total remaining profile " + str(profiles_left)
		rand_index = random.randint(0, profiles_left - 1)
		rand_obj = db.profiles.find({"$and":[{ "pubmed_crawledv2" : { "$exists" : False }}, {"citation_count":{"$gt":1}}]}).limit(-1).skip(rand_index).next()
		author_name = rand_obj['author_name']
		rand_label_key = rand_obj['_id']
		
		try:
			print "Details crawling for ",author_name
			complete_list = pubmed.fetch(author_name)
			author_pub_obj = {}
			author_pub_obj['_id'] = rand_label_key
			author_pub_obj['name'] = author_name
			author_pub_obj['complete'] = complete_list
			db.pubmed_profiles.insert(author_pub_obj)
			time.sleep(1)
			print "Going to crawl time series"
			for title,pubid,year,pubid in complete_list:
				print "PubId:",pubid
				already_present_or_not = db.citation_ts.find({"_id":pubid}).count()
				if already_present_or_not==0:
					citation_ts = pubmed.get_citations_ts(pubid)
					time.sleep(1)
					citation_obj = {}
					print "Time series"
					citation_obj['_id'] = pubid
					citation_obj['ts'] = citation_ts
					citation_obj['author_key'] = rand_label_key
					citation_obj['author_name'] = author_name
					db.citation_ts.insert(citation_obj)

			db.profiles.update({"_id" :rand_label_key},{"$set" : {"pubmed_crawledv2":True}})
		except:
			print "Exception!"
			db.profiles.update({"_id" :rand_label_key},{"$set" : {"pubmed_crawledv2":False}})

if __name__ == "__main__":
	sys.exit(main())