#! /usr/bin/env python

import os
import sys

from urllib2 import Request, build_opener, HTTPCookieProcessor
from urllib import quote, unquote
from cookielib import MozillaCookieJar
import HTMLParser
import time
    
import numpy as np
from numpy import random
import itertools
import random
import urlparse

# Import BeautifulSoup -- try 4 first, fall back to older
#try:
from bs4 import BeautifulSoup
#except ImportError:
#try:
#        from BeautifulSoup import BeautifulSoup
#except ImportError:
#        print('We need BeautifulSoup, sorry...')
#        sys.exit(1)

from pymongo import MongoClient

client = MongoClient()
db = client.citations_project

# Support unicode in both Python 2 and 3. In Python 3, unicode is str.
if sys.version_info[0] == 3:
	unicode = str # pylint: disable-msg=W0622
	encode = lambda s: unicode(s) # pylint: disable-msg=C0103
else:
	def encode(s):
		if isinstance(s, basestring):
			return s.encode('utf-8') # pylint: disable-msg=C0103
		else:
			return str(s)


class Error(Exception):
	"""Base class for any Scholar error."""


class FormatError(Error):
	"""A query argument or setting was formatted incorrectly."""


class QueryArgumentError(Error):
	"""A query did not have a suitable set of arguments."""


class ScholarConf(object):
	"""Helper class for global settings."""

	VERSION = '2.10'
	LOG_LEVEL = 1
	MAX_PAGE_RESULTS = 20 # Current maximum for per-page results
	SCHOLAR_SITE = 'http://scholar.google.com'

	# USER_AGENT = 'Mozilla/5.0 (X11; U; FreeBSD i386; en-US; rv:1.9.2.9) Gecko/20100913 Firefox/3.6.9'
	# Let's update at this point (3/14):
	USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64; rv:27.0) Gecko/20100101 Firefox/27.0'

	# If set, we will use this file to read/save cookies to enable
	# cookie use across sessions.
	COOKIE_JAR_FILE = None

class ScholarUtils(object):
	"""A wrapper for various utensils that come in handy."""

	LOG_LEVELS = {'error': 1,
				  'warn':  2,
				  'info':  3,
				  'debug': 4}

	@staticmethod
	def ensure_int(arg, msg=None):
		try:
			return int(arg)
		except ValueError:
			raise FormatError(msg)

	@staticmethod
	def log(level, msg):
		if level not in ScholarUtils.LOG_LEVELS.keys():
			return
		if ScholarUtils.LOG_LEVELS[level] > ScholarConf.LOG_LEVEL:
			return
		sys.stderr.write('[%5s]  %s' % (level.upper(), msg + '\n'))
		sys.stderr.flush()

class ScholarQuery(object):
	"""
	The base class for any kind of results query we send to Scholar.
	"""
	def __init__(self):
		self.url = None

		# The number of results requested from Scholar -- not the
		# total number of results it reports (the latter gets stored
		# in attrs, see below).
		self.num_results = ScholarConf.MAX_PAGE_RESULTS

		# Queries may have global result attributes, similar to
		# per-article attributes in ScholarArticle. The exact set of
		# attributes may differ by query type, but they all share the
		# basic data structure:
		self.attrs = {}

	def set_num_page_results(self, num_page_results):
		msg = 'maximum number of results on page must be numeric'
		self.num_results = ScholarUtils.ensure_int(num_page_results, msg)

	def get_url(self):
		"""
		Returns a complete, submittable URL string for this particular
		query instance. The URL and its arguments will vary depending
		on the query.
		"""
		return None

	def _add_attribute_type(self, key, label, default_value=None):
		"""
		Adds a new type of attribute to the list of attributes
		understood by this query. Meant to be used by the constructors
		in derived classes.
		"""
		if len(self.attrs) == 0:
			self.attrs[key] = [default_value, label, 0]
			return
		idx = max([item[2] for item in self.attrs.values()]) + 1
		self.attrs[key] = [default_value, label, idx]

	def __getitem__(self, key):
		"""Getter for attribute value. Returns None if no such key."""
		if key in self.attrs:
			return self.attrs[key][0]
		return None

	def __setitem__(self, key, item):
		"""Setter for attribute value. Does nothing if no such key."""
		if key in self.attrs:
			self.attrs[key][0] = item

	def _parenthesize_phrases(self, query):
		"""
		Turns a query string containing comma-separated phrases into a
		space-separated list of tokens, quoted if containing
		whitespace. For example, input

		  'some words, foo, bar'

		becomes

		  '"some words" foo bar'

		This comes in handy during the composition of certain queries.
		"""
		if query.find(',') < 0:
			return query
		phrases = []
		for phrase in query.split(','):
			phrase = phrase.strip()
			if phrase.find(' ') > 0:
				phrase = '"' + phrase + '"'
			phrases.append(phrase)
		return ' '.join(phrases)


class SearchScholarQuery(ScholarQuery):
	"""
	This version represents the search query parameters the user can
	configure on the Scholar website, in the advanced search options.
	"""
	SCHOLAR_QUERY_URL = ScholarConf.SCHOLAR_SITE + '/scholar?' \
		+ 'as_q=%(words)s' \
		+ '&as_epq=%(phrase)s' \
		+ '&as_oq=%(words_some)s' \
		+ '&as_eq=%(words_none)s' \
		+ '&as_occt=%(scope)s' \
		+ '&as_sauthors=%(authors)s' \
#		+ '&as_publication=%(pub)s' \
#		+ '&as_ylo=%(ylo)s' \
#		+ '&as_yhi=%(yhi)s' \
#		+ '&as_sdt=%(patents)s%%2C5' \
#		+ '&as_vis=%(citations)s' \
#		+ '&btnG=&hl=en' \
#		+ '&num=%(num)s'

	def __init__(self):
		ScholarQuery.__init__(self)
		self._add_attribute_type('num_results', 'Results', 0)
		self.words = None # The default search behavior
		self.words_some = None # At least one of those words
		self.words_none = None # None of these words
		self.phrase = None
		self.scope_title = False # If True, search in title only
		self.author = None
		self.pub = None
		self.timeframe = [None, None]
		self.include_patents = True
		self.include_citations = True

	def set_words(self, words):
		"""Sets words that *all* must be found in the result."""
		self.words = words

	def set_words_some(self, words):
		"""Sets words of which *at least one* must be found in result."""
		self.words_some = words

	def set_words_none(self, words):
		"""Sets words of which *none* must be found in the result."""
		self.words_none = words

	def set_phrase(self, phrase):
		"""Sets phrase that must be found in the result exactly."""
		self.phrase = phrase

	def set_scope(self, title_only):
		"""
		Sets Boolean indicating whether to search entire article or title
		only.
		"""
		self.scope_title = title_only

	def set_author(self, author):
		"""Sets names that must be on the result's author list."""
		self.author = author

	def set_pub(self, pub):
		"""Sets the publication in which the result must be found."""
		self.pub = pub

	def set_timeframe(self, start=None, end=None):
		"""
		Sets timeframe (in years as integer) in which result must have
		appeared. It's fine to specify just start or end, or both.
		"""
		if start:
			start = ScholarUtils.ensure_int(start)
		if end:
			end = ScholarUtils.ensure_int(end)
		self.timeframe = [start, end]

	def set_include_citations(self, yesorno):
		self.include_citations = yesorno

	def set_include_patents(self, yesorno):
		self.include_patents = yesorno

	def get_url(self):
		if self.words is None and self.words_some is None \
		   and self.words_none is None and self.phrase is None \
		   and self.author is None and self.pub is None \
		   and self.timeframe[0] is None and self.timeframe[1] is None:
			raise QueryArgumentError('search query needs more parameters')

		# If we have some-words or none-words lists, we need to
		# process them so GS understands them. For simple
		# space-separeted word lists, there's nothing to do. For lists
		# of phrases we have to ensure quotations around the phrases,
		# separating them by whitespace.
		words_some = None
		words_none = None

		if self.words_some:
			words_some = self._parenthesize_phrases(self.words_some)
		if self.words_none:
			words_none = self._parenthesize_phrases(self.words_none)

		urlargs = {'words': self.words or '',
				   'words_some': words_some or '',
				   'words_none': words_none or '',
				   'phrase': self.phrase or '',
				   'scope': 'title' if self.scope_title else 'any',
				   'authors': self.author or '',
				   'pub': self.pub or '',
				   'ylo': self.timeframe[0] or '',
				   'yhi': self.timeframe[1] or '',
				   'patents': '0' if self.include_patents else '1',
				   'citations': '0' if self.include_citations else '1',
				   'num': self.num_results or ScholarConf.MAX_PAGE_RESULTS}

		for key, val in urlargs.items():
			urlargs[key] = quote(encode(val))

		return self.SCHOLAR_QUERY_URL % urlargs

class PaperCitationQuery(ScholarQuery):

	def __init__(self):
		ScholarQuery.__init__(self)
		self.url = None

	def set_url(self, url):
		self.url = url


	def get_url(self):
		return 'https://scholar.google.com'+self.url

class ScholarLabelwiseQuery(ScholarQuery):
	"""
	This version represents the option to directly list of profiles based on label
	https://scholar.google.com/citations?view_op=search_authors&hl=en&mauthors=label:algorithms
	"""
	SCHOLAR_QUERY_URL = ScholarConf.SCHOLAR_SITE + '/citations?view_op=search_authors&hl=en' \
		'&mauthors=label:%(label)s'

	def __init__(self):
		ScholarQuery.__init__(self)
		self.label = None

	def set_label(self, label):
		self.label = label

	def get_url(self):

		urlargs = {'label': self.label or 'THERE_IS_AN_ERROR'}

		for key, val in urlargs.items():
			urlargs[key] = quote(encode(val))

		return self.SCHOLAR_QUERY_URL % urlargs

class ScholarLabelwiseNextPageQuery(ScholarQuery):
	"""
	This version represents the option to navigate to next page when we search based on a label
	https://scholar.google.com/citations?view_op=search_authors&hl=en&mauthors=label:algorithms&after_author=Jf9rAJ1Q__8J&astart=10
	"""
	SCHOLAR_QUERY_URL = ScholarConf.SCHOLAR_SITE + '%(nextpageurl)s'

	def __init__(self):
		ScholarQuery.__init__(self)
		self.nextpageurl = None

	def set_nextpageurl(self, nextpageurl):
		self.nextpageurl = nextpageurl

	def get_url(self):

		urlargs = {'nextpageurl': self.nextpageurl or 'THERE_IS_AN_ERROR'}

		for key, val in urlargs.items():
			urlargs[key] = val

		return self.SCHOLAR_QUERY_URL % urlargs

class ScholarLabelwiseQuerier(object):

    def __init__(self):
        self.profiles = []
        self.query = None
        self.nextpageurl = None
        self.shouldcontinue = True
        self.unique_labels = {}
        self.cjar = MozillaCookieJar()

        # If we have a cookie file, load it:
        if ScholarConf.COOKIE_JAR_FILE and \
                os.path.exists(ScholarConf.COOKIE_JAR_FILE):
            try:
                self.cjar.load(ScholarConf.COOKIE_JAR_FILE,
                               ignore_discard=True)
                ScholarUtils.log('info', 'loaded cookies file')
            except Exception as msg:
                ScholarUtils.log('warn', 'could not load cookies file: %s' % msg)
                self.cjar = MozillaCookieJar()  # Just to be safe

        self.opener = build_opener(HTTPCookieProcessor(self.cjar))
        self.settings = None  # Last settings object, if any

    def send_query(self, query):
        """
		This method initiates a search query (a ScholarQuery instance)
		with subsequent parsing of the response.
		"""
        #		self.clear_articles()
        self.query = query
        self.articles = []
        html = self._get_http_response(url=query.get_url(),
                                       log_msg='dump of query response HTML',
                                       err_msg='results retrieval failed')
        if html is None:
            return

        self.profiles = self.extract_info(html)

    def save_cookies(self):
        """
		This stores the latest cookies we're using to disk, for reuse in a
		later session.
		"""
        if ScholarConf.COOKIE_JAR_FILE is None:
            return False
        try:
            self.cjar.save(ScholarConf.COOKIE_JAR_FILE,
                           ignore_discard=True)
            ScholarUtils.log('info', 'saved cookies file')
            return True
        except Exception as msg:
            ScholarUtils.log('warn', 'could not save cookies file: %s' % msg)
            return False

    def _get_http_response(self, url, log_msg=None, err_msg=None):
        """
		Helper method, sends HTTP request and returns response payload.
		"""
        if log_msg is None:
            log_msg = 'HTTP response data follow'
        if err_msg is None:
            err_msg = 'request failed'
        try:
            ScholarUtils.log('info', 'requesting %s' % unquote(url))
            print url
            req = Request(url=url, headers={'User-Agent': ScholarConf.USER_AGENT})
            hdl = self.opener.open(req)
            html = hdl.read()

            ScholarUtils.log('debug', log_msg)
            ScholarUtils.log('debug', '>>>>' + '-' * 68)
            ScholarUtils.log('debug', 'url: %s' % hdl.geturl())
            ScholarUtils.log('debug', 'result: %s' % hdl.getcode())
            ScholarUtils.log('debug', 'headers:\n' + str(hdl.info()))
            ScholarUtils.log('debug', 'data:\n' + html.decode('utf-8'))  # For Python 3
            ScholarUtils.log('debug', '<<<<' + '-' * 68)

            return html
        except Exception as err:
            ScholarUtils.log('info', err_msg + ': %s' % err)
            return None

    def extract_info(self, html):
#		soup = BeautifulSoup(html,"html.parser")
#                print "html:",html
                soup = BeautifulSoup(html,"html.parser")
		if soup is None:
			return [], None
		profileDivs = soup.findAll("div", class_="gsc_1usr_text")
		profiles = []
		new_unique_labels = {}
		for profile in profileDivs:
			profile_dict = {}

			author_name_node = profile.select('.gsc_1usr_name a')
			author_href = ''
			if author_name_node:
				author_name = author_name_node[0].string.encode('utf-8').strip()
				author_href = author_name_node[0]['href']
				parsed_result = urlparse.urlparse(author_href)
				author_id_orig = urlparse.parse_qs(parsed_result.query)['user'][0]
				author_id = author_id_orig.encode("utf-8")
			else:
				continue

			author_aff_node = profile.find('div', {'class': 'gsc_1usr_aff'})
			if author_aff_node is not None:
				author_aff = author_aff_node.string
			else:
				author_aff = ''

			author_eml_node = profile.find('div', {'class': 'gsc_1usr_eml'})
			if author_eml_node is not None:
				author_eml = author_eml_node.string
			else:
				author_eml = ''

			author_emlb_node = profile.find('div', {'class': 'gsc_1usr_emlb'})
			if author_emlb_node is not None:
				author_emlb = author_emlb_node.string
			else:
				author_emlb = ''

			author_cby_node = profile.find('div', {'class': 'gsc_1usr_cby'})
			if author_cby_node is not None:
				author_cby = author_cby_node.string
			else:
				author_cby = '0'

			all_labels = profile.findAll("a", class_="gsc_co_int")
			prof_labels = []
			for label_node in all_labels:
				new_unique_labels[label_node.string] = label_node['href']
				prof_labels.append(label_node.string)	
			
			citation_count = int(author_cby.replace('Cited by ', ''))
			if citation_count < 40:
				self.shouldcontinue = False

			profile_dict['author_name'] = author_name
			profile_dict['author_href'] = author_href
			profile_dict['author_aff'] = author_aff
			profile_dict['author_eml'] = author_eml
			profile_dict['author_emlb'] = author_emlb
			profile_dict['author_cby'] = author_cby
			profile_dict['prof_labels'] = prof_labels
			profile_dict['citation_count'] = citation_count
			profile_dict['_id'] = author_id
			profiles.append(profile_dict)
		self.unique_labels = new_unique_labels
		print 'profiles ended'
		next_button = soup.select('button.gs_btnPR.gs_in_ib.gs_btn_half.gs_btn_srt')
		
		if next_button and next_button[0].has_attr('onclick') and next_button[0]['onclick'] is not None:
			onclickurl = next_button[0]['onclick']
			href = onclickurl.replace('window.location=', '')
			href = href.replace('\'','')
			href = href.replace('\\x','%')
			self.nextpageurl = str(href)
		else:
			self.shouldcontinue = False
		return profiles

class ScholarProfileQuery(ScholarQuery):
	"""
	This version represents the option to directly query a user's profile
	on google scholar.
	https://scholar.google.com/citations?user=fnE2dSoAAAAJ&cstart=20&pagesize=20
	"""
	SCHOLAR_QUERY_URL = ScholarConf.SCHOLAR_SITE + '/citations?' \
		+ 'user=%(user)s' \
		+ '&cstart=%(cstart)s' \
		+ '&pagesize=%(page_size)s'

	def __init__(self):
		ScholarQuery.__init__(self)
		self.user = None
		self.cstart = None
		self.page_size = None

	def set_user(self, user_id):
		self.user = user_id

	def set_cstart(self, start_index):
		"""Sets words of which *at least one* must be found in result."""
		self.cstart = start_index

	def set_page_size(self, page_len):
		"""Sets words of which *none* must be found in the result."""
		self.page_size = page_len

	def get_url(self):

		urlargs = {'user': self.user or 'THERE_IS_AN_ERROR',
				   'cstart': self.cstart or 'THERE_IS_AN_ERROR',
				   'page_size': self.page_size or 'THERE_IS_AN_ERROR'}

		for key, val in urlargs.items():
			urlargs[key] = quote(encode(val))

		return self.SCHOLAR_QUERY_URL % urlargs

class ScholarProfileQuerier(object):

    def __init__(self):
        self.articles = []
        self.citation_timeline = []
        self.tags = []
        self.last_fetch_count = 0
        self.query = None
        self.cjar = MozillaCookieJar()

        # If we have a cookie file, load it:
        if ScholarConf.COOKIE_JAR_FILE and \
                os.path.exists(ScholarConf.COOKIE_JAR_FILE):
            try:
                self.cjar.load(ScholarConf.COOKIE_JAR_FILE,
                               ignore_discard=True)
                ScholarUtils.log('info', 'loaded cookies file')
            except Exception as msg:
                ScholarUtils.log('warn', 'could not load cookies file: %s' % msg)
                self.cjar = MozillaCookieJar()  # Just to be safe

        self.opener = build_opener(HTTPCookieProcessor(self.cjar))
        self.settings = None  # Last settings object, if any

    def send_query(self, query):
        """
		This method initiates a search query (a ScholarQuery instance)
		with subsequent parsing of the response.
		"""
        #		self.clear_articles()
        self.query = query
        self.articles = []
        self.citation_timeline = []
        html = self._get_http_response(url=query.get_url(),
                                       log_msg='dump of query response HTML',
                                       err_msg='results retrieval failed')
        if html is None:
            return
        #self.citation_timeline.extend(self.extract_citation_timeline(html))
        self.articles.extend(self.extract_info(html))

    def save_cookies(self):
        """
		This stores the latest cookies we're using to disk, for reuse in a
		later session.
		"""
        if ScholarConf.COOKIE_JAR_FILE is None:
            return False
        try:
            self.cjar.save(ScholarConf.COOKIE_JAR_FILE,
                           ignore_discard=True)
            ScholarUtils.log('info', 'saved cookies file')
            return True
        except Exception as msg:
            ScholarUtils.log('warn', 'could not save cookies file: %s' % msg)
            return False

    def _get_http_response(self, url, log_msg=None, err_msg=None):
        """
		Helper method, sends HTTP request and returns response payload.
		"""
        if log_msg is None:
            log_msg = 'HTTP response data follow'
        if err_msg is None:
            err_msg = 'request failed'
        try:
            ScholarUtils.log('info', 'requesting %s' % unquote(url))

            req = Request(url=url, headers={'User-Agent': ScholarConf.USER_AGENT})
            hdl = self.opener.open(req)
            html = hdl.read()

            ScholarUtils.log('debug', log_msg)
            ScholarUtils.log('debug', '>>>>' + '-' * 68)
            ScholarUtils.log('debug', 'url: %s' % hdl.geturl())
            ScholarUtils.log('debug', 'result: %s' % hdl.getcode())
            ScholarUtils.log('debug', 'headers:\n' + str(hdl.info()))
            ScholarUtils.log('debug', 'data:\n' + html.decode('utf-8'))  # For Python 3
            ScholarUtils.log('debug', '<<<<' + '-' * 68)

            return html
        except Exception as err:
            ScholarUtils.log('info', err_msg + ': %s' % err)
            return None

    def extract_citation_timeline(self, html):
    	soup = BeautifulSoup(html, "html.parser")
    	years = []
    	counts = []
    	citations_section = soup.find('div', {'id': 'gsc_g'})
    	if citations_section is not None:
	    	years_spans = citations_section.findAll('span', {'class': 'gsc_g_t'})
	    	for year_span in years_spans:
	    		years.append(int(year_span.text))
	    	citaton_count_spans = citations_section.findAll('span', {'class': 'gsc_g_al'})
	    	for citation_span in citaton_count_spans:
	    		counts.append(int(citation_span.text))
    	return zip(years, counts)

    def extract_info(self, html):
#		soup = BeautifulSoup(html,"html.parser")
#                print "html",html
                soup = BeautifulSoup(html,"html.parser")
		if soup is None:
			return [], None
                tags_list = soup.find_all("div", class_="gsc_prf_il")
                if len(tags_list) != 0:
                        tags = tags_list[1].find_all("a", class_="gsc_prf_ila")
                        for tag in tags:
                                print "tag.get_text():",tag.get_text()
                                self.tags.append(tag.get_text())
		papers = soup.findAll("tr", class_="gsc_a_tr")
		publication_details = []
		for paper in papers:
			try:
				paper_name_node = paper.find('a', {'class': 'gsc_a_at'})
				citations_href = ''
				if paper_name_node is not None:
					paper_name = paper_name_node.string.encode('utf-8').strip()
					citations_href = paper_name_node['href']
				else:
					continue
				citation_count_node = paper.find('a', {'class': 'gsc_a_ac'})
				if citation_count_node is not None and len(citation_count_node.string) != 0:
					try:
						citation_count = int(citation_count_node.string)
					except Exception:
						citation_count = 0
				else:
					citation_count = 0
				year_node = paper.find('span', {'class': 'gsc_a_h'})
				if year_node is not None and year_node.string is not None:
					year = int(year_node.string)
				else:
					continue
			except:
				print "Exception in ", str(paper),". Not adding."
				continue
			publication_details.append([paper_name,citation_count,year,citations_href])
		self.last_fetch_count = len(publication_details)
		return publication_details

class ScholarQuerier(object):

    """
    ScholarQuerier instances can conduct a search on Google Scholar
    with subsequent parsing of the resulting HTML content.  The
    articles found are collected in the articles member, a list of
    ScholarArticle instances.
    """

    def __init__(self):
        self.articles = []
        self.query = None
        self.cjar = MozillaCookieJar()
        self.result_count = 0
        self.author_id = ''

        # If we have a cookie file, load it:
        if ScholarConf.COOKIE_JAR_FILE and \
           os.path.exists(ScholarConf.COOKIE_JAR_FILE):
            try:
                self.cjar.load(ScholarConf.COOKIE_JAR_FILE,
                               ignore_discard=True)
                ScholarUtils.log('info', 'loaded cookies file')
            except Exception as msg:
                ScholarUtils.log('warn', 'could not load cookies file: %s' % msg)
                self.cjar = MozillaCookieJar() # Just to be safe

        self.opener = build_opener(HTTPCookieProcessor(self.cjar))
        self.settings = None # Last settings object, if any

    def send_query(self, query):
        """
        This method initiates a search query (a ScholarQuery instance)
        with subsequent parsing of the response.
        """
        self.query = query
        print "url:",query.get_url()
        html = self._get_http_response(url=query.get_url(),
                                       log_msg='dump of query response HTML',
                                       err_msg='results retrieval failed')
#        print "html",html
        if html is None:
            return

        self.parse(html)

    def parse(self, html):
        """
        This method allows parsing of provided HTML content.
        """
        #write the parser for user_id here.
#        print "html:",html

#        soup = BeautifulSoup(html,"html.parser")
        print "before parsing"
        soup = BeautifulSoup(html,"html.parser")
        print "after parsing"
        search_results = soup.findAll("h4", class_="gs_rt2")
        print "search_results:",search_results
        self.result_count = 0
        self.author_id = ''
        for result in search_results:
            author=result.find("a")
            if author is not None:
                link_href = author['href']
                first_half = link_href[link_href.find('user=')+len('user='):]
                self.author_id = first_half[:first_half.index('&')]
                self.result_count=self.result_count+1
        print "author_id:",self.author_id

    def save_cookies(self):
        """
        This stores the latest cookies we're using to disk, for reuse in a
        later session.
        """
        if ScholarConf.COOKIE_JAR_FILE is None:
            return False
        try:
            self.cjar.save(ScholarConf.COOKIE_JAR_FILE,
                           ignore_discard=True)
            ScholarUtils.log('info', 'saved cookies file')
            return True
        except Exception as msg:
            ScholarUtils.log('warn', 'could not save cookies file: %s' % msg)
            return False

    def _get_http_response(self, url, log_msg=None, err_msg=None):
        """
        Helper method, sends HTTP request and returns response payload.
        """
        if log_msg is None:
            log_msg = 'HTTP response data follow'
        if err_msg is None:
            err_msg = 'request failed'
        try:
            ScholarUtils.log('info', 'requesting %s' % unquote(url))

            req = Request(url=url, headers={'User-Agent': ScholarConf.USER_AGENT})
            hdl = self.opener.open(req)
            html = hdl.read()

            ScholarUtils.log('debug', log_msg)
            ScholarUtils.log('debug', '>>>>' + '-'*68)
            ScholarUtils.log('debug', 'url: %s' % hdl.geturl())
            ScholarUtils.log('debug', 'result: %s' % hdl.getcode())
            ScholarUtils.log('debug', 'headers:\n' + str(hdl.info()))
            ScholarUtils.log('debug', 'data:\n' + html.decode('utf-8')) # For Python 3
            ScholarUtils.log('debug', '<<<<' + '-'*68)

            return html
        except Exception as err:
            ScholarUtils.log('info', err_msg + ': %s' % err)
            return None


class PublicationCitationQuerier(object):
	def __init__(self):
		self.query = None
		self.cjar = MozillaCookieJar()
		self.paper_details = []

		# If we have a cookie file, load it:
		if ScholarConf.COOKIE_JAR_FILE and \
				os.path.exists(ScholarConf.COOKIE_JAR_FILE):
			try:
				self.cjar.load(ScholarConf.COOKIE_JAR_FILE,
							   ignore_discard=True)
				ScholarUtils.log('info', 'loaded cookies file')
			except Exception as msg:
				ScholarUtils.log('warn', 'could not load cookies file: %s' % msg)
				self.cjar = MozillaCookieJar()  # Just to be safe

		self.opener = build_opener(HTTPCookieProcessor(self.cjar))
		self.settings = None  # Last settings object, if any

	def send_query(self, query):
		"""
		This method initiates a search query (a ScholarQuery instance)
		with subsequent parsing of the response.
		"""
		self.query = query
		print 'url :', query.get_url()

		html = self._get_http_response(url=query.get_url(),
										   log_msg='dump of query response HTML',
										   err_msg='results retrieval failed')

		if html is None:
			return

		self.parse(html)

	def parse(self, html):
		"""
		This method allows parsing of provided HTML content.
		"""
		# write the parser for user_id here.
		soup = BeautifulSoup(html, "html.parser")

		f = soup.find('div', id="gsc_graph")
		if not f:
			return []

		years_ = f.findAll("span", class_="gsc_g_t")
		counts_ = f.findAll("span", class_="gsc_g_al")
		years = [int(y.contents[0]) for y in years_]
		counts = [int(c.contents[0]) for c in counts_]
		paper_name = ""

		alt_link = soup.find("a", class_="gsc_title_link")
		if alt_link:
			paper_name = alt_link.contents[0]
			print "Paper name", paper_name

		self.paper_details = [paper_name,zip(years, counts)]

	def save_cookies(self):
		"""
		This stores the latest cookies we're using to disk, for reuse in a
		later session.
		"""
		if ScholarConf.COOKIE_JAR_FILE is None:
			return False
		try:
			self.cjar.save(ScholarConf.COOKIE_JAR_FILE,
						   ignore_discard=True)
			ScholarUtils.log('info', 'saved cookies file')
			return True
		except Exception as msg:
			ScholarUtils.log('warn', 'could not save cookies file: %s' % msg)
			return False

	def _get_http_response(self, url, log_msg=None, err_msg=None):
		"""
		Helper method, sends HTTP request and returns response payload.
		"""
		if log_msg is None:
			log_msg = 'HTTP response data follow'
		if err_msg is None:
			err_msg = 'request failed'
		try:
			ScholarUtils.log('info', 'requesting %s' % unquote(url))

			req = Request(url=url, headers={'User-Agent': ScholarConf.USER_AGENT})
			hdl = self.opener.open(req)
			html = hdl.read()

			ScholarUtils.log('debug', log_msg)
			ScholarUtils.log('debug', '>>>>' + '-' * 68)
			ScholarUtils.log('debug', 'url: %s' % hdl.geturl())
			ScholarUtils.log('debug', 'result: %s' % hdl.getcode())
			ScholarUtils.log('debug', 'headers:\n' + str(hdl.info()))
			ScholarUtils.log('debug', 'data:\n' + html.decode('utf-8'))  # For Python 3
			ScholarUtils.log('debug', '<<<<' + '-' * 68)

			return html
		except Exception as err:
			ScholarUtils.log('info', err_msg + ': %s' % err)
			return None


def get_citations_ts(paper_link):
	print 'Inside get_citations_ts'
	query = PaperCitationQuery()
	query.set_url(paper_link)
	querier = PublicationCitationQuerier()
	querier.send_query(query)
	return querier.paper_details

def fetch_all_for_this_user(author):
    querier = ScholarProfileQuerier()
    query = ScholarProfileQuery()
    query.set_user(author)
    fetched_till_now = 0
    complete_list=[]
    while True:
        query.set_cstart(fetched_till_now)
        query.set_page_size(100)
        querier.send_query(query)
        fetched_till_now = fetched_till_now+100
        complete_list.extend(querier.articles)
        if querier.last_fetch_count <100:
            break
        time.sleep(5*random.random())
    return complete_list

def fetch_all_for_this_user_v2(author):
    querier = ScholarProfileQuerier()
    query = ScholarProfileQuery()
    query.set_user(author)
    fetched_till_now = 0
    complete_list=[]
    citations_list=[]
    tags=[]
    query.set_cstart(fetched_till_now)
    query.set_page_size(100)
    querier.send_query(query)
    fetched_till_now = fetched_till_now+100
    complete_list.extend(querier.articles)
    citations_list.extend(querier.citation_timeline)
    tags.extend(querier.tags)
#    time.sleep(4*random.random())
    return [complete_list, citations_list, tags]

def fetch(author_name):
    querier = ScholarQuerier()
    query = SearchScholarQuery()
    query.set_author(author_name)
    querier.send_query(query)
    return fetch_all_for_this_user(querier.author_id)

def fetch_v2(author_name):
    querier = ScholarQuerier()
    print "Author_name:",author_name
    query = SearchScholarQuery()
    query.set_author(author_name)
    querier.send_query(query)
    print "querier.author_id:",querier.author_id
    return [querier.author_id, fetch_all_for_this_user_v2(querier.author_id)]

def main():
        counter = 0
        random.seed(100000)
        print "Total number of items in scholar_profiles :", db.scholar_profiles.find().count()
	while True:
		total_users_remaining = db.profiles.find({"$and":[{ "scholarProfileCrawled" : { "$exists" : False }}]}).count()
#                random.seed(random.randint(0, total_users_remaining))
                if total_users_remaining % 1000 == 0 and counter!=0:
                        time.sleep(60*random.random())
		if total_users_remaining == 0:
			print "0 users remaining"
			break
		print "Total remaining users " + str(total_users_remaining)
		rand_index = random.randint(0, total_users_remaining - 1)
		rand_obj = db.profiles.find({"$and":[{ "scholarProfileCrawled" : { "$exists" : False }}]}).limit(-1).skip(rand_index).next()
                user_id = rand_obj['_id']
                print "User id now:", str(user_id)
                '''
                labels_of_user = rand_obj['prof_labels']
                mincount = 9999999999999999999
                for label in labels_of_user:
                        try:
                                count = db.profiles.find({"prof_labels":{"$in":[label]}}).count()
                                if count>50:
                                        mincount = min(count, mincount)
                        except:
                                print "Exception in fethcing for label ",label

                mincount = mincount/10
                if mincount == 0:
                        mincount = 1
                random.seed(mincount)
                '''
                randomizer = random.randint(0, 5)

                if randomizer == 0:
                        print "Object ******fetched****** after randomization"
                        [complete_list, citations_list] = fetch_all_for_this_user_v2(user_id)
                        new_obj_to_be_inserted = {}
                        new_obj_to_be_inserted['_id'] = user_id
                        new_obj_to_be_inserted['complete'] = complete_list
                        new_obj_to_be_inserted['citations'] = citations_list
                        db.scholar_profiles.insert(new_obj_to_be_inserted)
                        counter = counter + 1
                else:
                        print "Object ignored after randomization"
                if counter%100==0 or counter<5:
                        print "Total number of items in scholar_profiles :", db.scholar_profiles.find().count()
		db.profiles.update({"_id" :user_id},{"$set" : {"scholarProfileCrawled":"checkedAfterRandomizer"}})


def main123456():

	while True:
		total_labels = db.labels.find({"$and":[{ "processedV2" : { "$exists" : False }}, {"count":{"$gt":1}}]}).count()
		if total_labels == 0:
			break
		print "Total remaining labels " + str(total_labels)
		rand_index = random.randint(0, total_labels - 1)
		rand_obj = db.labels.find({"$and":[{ "processedV2" : { "$exists" : False }}, {"count":{"$gt":1}}]}).limit(-1).skip(rand_index).next()
		rand_label = rand_obj['label']
		rand_label_key = rand_obj['_id']
		parsed_result = urlparse.urlparse(rand_label_key)
		label_from_url = urlparse.parse_qs(parsed_result.query)['mauthors'][0]
		final_label = label_from_url.replace("label:", "")
		print "Rand label " + final_label
		print "Rand label key " + rand_label_key
		db.labels.update({"_id" :rand_label_key},{"$set" : {"processedV2":True}})
	
		# final_label = "music_performance"

		querier = ScholarLabelwiseQuerier()
		query = ScholarLabelwiseQuery()
		query.set_label(final_label)
		querier.send_query(query)
		for profile in querier.profiles:
			try:
				result = db.profiles.insert_one(profile)
				print "Inserted profile id is " + result.inserted_id
			except Exception as err:
				print "Profile not added due to Exception"

		for key in querier.unique_labels:
			label_obj = {}
			label_obj['_id'] = querier.unique_labels[key]
			label_obj['label'] = key
			label_obj['count'] = 1
			try:
				result = db.labels.insert_one(label_obj)
				print "Inserted label id is " + result.inserted_id
			except Exception as err:
				print "Label not added due to Exception"
				try:
					db.labels.update(
						{ '_id' : querier.unique_labels[key] },
						{ '$inc' : { 'count': 1} }
					)
				except Exception:
					print "Increment failed"

		while querier.shouldcontinue:
			time.sleep(5*random.random())
			query = ScholarLabelwiseNextPageQuery()
			print "Next Page :" + str(querier.nextpageurl)
			query.set_nextpageurl(str(querier.nextpageurl))
			querier.send_query(query)
			for profile in querier.profiles:
				try:
					result = db.profiles.insert_one(profile)
					print "Inserted profile id is " + result.inserted_id
				except Exception as err:
					print "Profile not added due to Exception"
					repr(err)
			for key in querier.unique_labels:
				label_obj = {}
				label_obj['_id'] = querier.unique_labels[key]
				label_obj['label'] = key
				label_obj['count'] = 1
				try:
					result = db.labels.insert_one(label_obj)
					print "Inserted label id is " + result.inserted_id
				except Exception as err:
					print "Label not added due to Exception"
				try:
					db.labels.update(
						{ '_id' : querier.unique_labels[key] },
						{ '$inc': { 'count' : 1} }
					)
				except Exception:
					print "Increment failed"
		# break

def main123():
    querier = ScholarLabelwiseQuerier()
    query = ScholarLabelwiseQuery()
    query.set_label('algorithms')
    querier.send_query(query)
    for profile in querier.profiles:
    	print profile
        exit()
    	try:
	    	result = db.profiles.insert_one(profile)
    		print "inserted id is " + result.inserted_id
    	except Exception as err:
    		print "Exception :" + str(err)
    print 'shouldcontinue :' + str(querier.shouldcontinue)
    print 'nextpageurl :' + str(querier.nextpageurl)
    print '---end---'
    time.sleep(5)
    newquery = ScholarLabelwiseNextPageQuery()
    newquery.set_nextpageurl(str(querier.nextpageurl))
    querier.send_query(newquery)
    for profile in querier.profiles:
    	print profile
    	try:
	    	result = db.profiles.insert_one(profile)
    		print "inserted id is " + result.inserted_id
    	except Exception as err:
    		print "Exception :" + str(err)

    print '---end---'
    time.sleep(5)
    query = ScholarLabelwiseQuery()
    query.set_label('algorithms')
    querier.send_query(query)
    for profile in querier.profiles:
    	print profile
    	try:
	    	result = db.profiles.insert_one(profile)
    		print "inserted id is " + result.inserted_id
    	except Exception as err:
    		print "Exception :" + str(err)
    print 'shouldcontinue :' + str(querier.shouldcontinue)
    print 'nextpageurl :' + str(querier.nextpageurl)
    print '---end---'
    time.sleep(5)
    newquery = ScholarLabelwiseNextPageQuery()
    newquery.set_nextpageurl(str(querier.nextpageurl))
    querier.send_query(newquery)
    for profile in querier.profiles:
    	print profile
    	try:
	    	result = db.profiles.insert_one(profile)
    		print "inserted id is " + result.inserted_id
    	except Exception as err:
    		print "Exception :" + str(err)
    print '---end---'

if __name__ == "__main__":
	sys.exit(main123())
