import sys

import numpy as np

from pymongo import MongoClient

client = MongoClient()
db = client.citations_project

def main():
	all_citation_count_time_series = []
	all_pub_count_time_series = []
	all_profiles = db.scholar_profiles.find({})
	skipped = 0
	allcount = 0
	for profile in all_profiles:
		allcount +=1
		if not profile['complete']:
			# print "Skipping ",profile
			skipped+=1
			continue
		# print "profile:",profile
		sorted_ts = sorted(profile['complete'], key=lambda x: x[2])
		yearwise_citation_sums = {}
		yearwise_pubcount_sums = {}
		for i in xrange(0, len(sorted_ts)):
			if sorted_ts[i][2] in yearwise_citation_sums:
				yearwise_citation_sums[sorted_ts[i][2]] += sorted_ts[i][1]
			else:
				yearwise_citation_sums[sorted_ts[i][2]] = sorted_ts[i][1]

			if sorted_ts[i][2] in yearwise_pubcount_sums:
				yearwise_pubcount_sums[sorted_ts[i][2]] += 1
			else:
				yearwise_pubcount_sums[sorted_ts[i][2]] = 1

		sorted_year_keys = sorted(yearwise_citation_sums.keys())
		beg_year = sorted_year_keys[0]
		end_year = sorted_year_keys[-1]

		beg_index = 0 + 1
		while beg_index < len(sorted_year_keys):
			if sorted_year_keys[beg_index] - beg_year > 5:
				beg_year = sorted_year_keys[beg_index]
				beg_index = beg_index + 1
			else:
				break

		if end_year-beg_year>100:
			print "year diff gt 50. ", beg_year,"->",end_year, "diff:",end_year-beg_year," profile:",profile['_id']
		# print "beg_year:",beg_year
		# print "end_year:",end_year
		year_index = 1
		citation_count_data = {}
		yearwise_count_data = {}
		for year in xrange(beg_year, end_year+1):
			if year in yearwise_citation_sums:
				citation_count_data[year_index] = yearwise_citation_sums[year]
			else:
				citation_count_data[year_index] = 0

			if year in yearwise_pubcount_sums:
				yearwise_count_data[year_index] = yearwise_pubcount_sums[year]
			else:
				yearwise_count_data[year_index] = 0
			year_index = year_index + 1

		# print "citation_count_data:",citation_count_data
		# print "yearwise_count_data:",yearwise_count_data
		all_citation_count_time_series.append(citation_count_data)
		all_pub_count_time_series.append(yearwise_count_data)
		# break

	grouped_citations_by_time_from_beg = {}
	for time_series in all_citation_count_time_series:
		for key,val in time_series.iteritems():
			if key in grouped_citations_by_time_from_beg:
				grouped_citations_by_time_from_beg[key].append(val)
			else:
				grouped_citations_by_time_from_beg[key] = [val]

	grouped_pubcounts_by_time_from_beg = {}
	for time_series in all_pub_count_time_series:
		for key,val in time_series.iteritems():
			if key in grouped_pubcounts_by_time_from_beg:
				grouped_pubcounts_by_time_from_beg[key].append(val)
			else:
				grouped_pubcounts_by_time_from_beg[key] = [val]

	means = {}
	freqs = {}
	stds = {}
	medians = {}
	for key,val in grouped_pubcounts_by_time_from_beg.iteritems():
		means[key] = np.mean(val)
		freqs[key] = len(val)
		stds[key] = np.std(val)
		medians[key] = np.median(val)

	print "All profile count :",allcount
	print "Total skipped profiles :",skipped

	print "Means"
	print means
	print "Freqs"
	print freqs
	print "Stds"
	print stds
	print "Median"
	print medians

if __name__ == "__main__":
	sys.exit(main())