from app import app
from flask import render_template, redirect, url_for
from .forms import QueryForm
from .forms import PaperDetailsHiddenForm
from flask import session
import scholarV2 as scholar
#import pubmed
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import pandas as pd
import numpy as np
from timeseries_api import AuthorForecaster
from pymongo import MongoClient
import ast

client = MongoClient()
db = client.citations_project

@app.route('/')
@app.route('/query_form', methods=["GET", "POST"])
def query():
    form = QueryForm()
    print 'Reached here'
    if form.validate_on_submit():
        session['author_requested'] = form.author_name.data
        session['source_requested'] = form.source.data
        return redirect(url_for('index'))
    print 'Validation failed'
    return render_template('form.html', form=form)

@app.route('/citations_plot', methods=["GET", "POST"])
def get_citations():
    form = PaperDetailsHiddenForm()
    print 'Inside get_citations()'
    session['paper_link'] = form.paper_link.data
    return redirect(url_for('get_paper_details'))

@app.route('/paper_details', methods=["GET", "POST"])
def get_paper_details():
    paper_link_from_session = session['paper_link']
    if session['source_requested']=="pubmed":
      details=pubmed.get_citations_ts(paper_link_from_session)
    else:
      details=scholar.get_citations_ts(paper_link_from_session)
    
    years=[]
    year_vals=[]
    for i in details[1]:
        years.append(i[0])
        year_vals.append(i[1])

    original_df = pd.DataFrame().from_records(zip(years, year_vals), columns=['time', 'values'], index=['time'])
    a = AuthorForecaster()
    forecast_df = a.predict_number_citations(original_df)

    new_vals = list(forecast_df['values'].values)
    new_years = list(forecast_df.index.values)

    return render_template('paper_details.html', paper_details_link=paper_link_from_session,
                           paper_name=details[0],years=years,year_vals=year_vals,new_vals=new_vals,
                           new_years=new_years)

@app.route('/index')
def index():
    author_requested = session['author_requested']
#    profile_keys = db.profiles.find({"author_name":author_requested})
#    print profile_keys[0]
#    complete_profile = db.scholar_profiles.find({"_id" : profile_keys[0]["_id"]})
#    print complete_profile
#    complete_list = complete_profile[0]["complete"]
    if session['source_requested']=="pubmed":
       complete_list=pubmed.fetch(author_requested)
    else:
       return_val = scholar.fetch_v2(author_requested)
       author_id = return_val[0]
       complete_list = return_val[1][0]
       tags = return_val[1][2]
       print "tags:",tags
       if not complete_list:
           return redirect(url_for('query'))
    yearwise={}
    for item in complete_list:
        if item[2] in yearwise:
            count=yearwise[item[2]]
            count=count+1
            yearwise[item[2]]=count
        else:
            yearwise[item[2]]=1
    years_list = yearwise.keys()
    years_list.sort()
    years_len = len(years_list)
    print "years_list b4:",years_list
    # print "max(years_list)+1:",max(years_list)+1
    # print "max(years_list) + 45-years_len:",max(years_list) + 45-years_len
    # for y in np.arange(max(years_list)+1, max(years_list) + 45-years_len):
    #     print "y:",y
    #     years_list.append(y)
    yearwise_forui=[]
    yearwise_vals = []
    print "years_list:",years_list
    for year in years_list:
        if year in yearwise:
            yearwise_forui.append([year,yearwise[year]])
            yearwise_vals.append(yearwise[year])

    original_df = pd.DataFrame().from_records(zip(years_list[0:len(yearwise_vals)], yearwise_vals), columns=['time', 'values'], index=['time'])
    # print original_df
    a = AuthorForecaster()
    forecast_df = a.predict_using_gompertz(original_df)
    new_vals = list(forecast_df['values'].values)
    new_years = list(forecast_df.index.values)
    print "new_years:",new_years
    print "years_list:",years_list
    # years_list.extend(new_years)
    print "yoyoyo years_list:",years_list
    complete_list=complete_list[:5]
    print "yearwise_forui:",yearwise_forui
    session['complete_ts'] = repr(yearwise_vals + new_vals)
    session['requested_author_id'] = author_id
    session['prof_labels'] = repr(tags)
    return render_template("result.html",
                           title="Results",
                           author_name=author_requested,
                           years_list=years_list,
                           original_data=yearwise_vals,
                           papers=complete_list,
                           new_vals=new_vals,
                           new_years=new_years,
                           author_id = author_id,
                           tags = ", ".join(tags)
                           )

@app.route('/kNearest')
def k_nearest():
    print "in func call"
    requested_author_id = session['requested_author_id']
    complete_ts = ast.literal_eval(session['complete_ts'])
    author_tags = ast.literal_eval(session['prof_labels'])
    author_requested = session['author_requested']
    a = AuthorForecaster()
    k_nearest = a.get_nearest_k(requested_author_id, complete_ts, author_tags)
    print "k_nearest:",k_nearest
    return render_template("nearest.html",
                               title="Closest neighbours",
                               author_name=author_requested,
                               neighbours=k_nearest
                               )
